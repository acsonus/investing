class Goal {
    constructor(name, currentprice, achieveyears, initialpayment, investmentreturn) {
        this.name = name;
        this.currentprice = currentprice;
        this.achieveyears = achieveyears;
        this.initialpayment  = initialpayment;
        this.investmentreturn = investmentreturn;
    }
    get getName(){
        return this.name;
    }
    get getCurrentPrice(){
        return this.currentprice;
    }
    get getAchieveYears(){
        return this.achieveyears;
    }
    get getInitialPayment(){
        return this.initialpayment;
    }

    get getInvestmentReturn(){
        return this.investmentreturn;
    }
}

module.exports = Goal