import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'configuration',
    loadChildren: () =>
      import('./pages/configuration/commonconfig/configuration.module').then(
        (m) => m.ConfigurationPageModule
      ),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./pages/dashboard/dashboard.module').then(
        (m) => m.DashboardPageModule
      ),
  },

  {
    path: 'limited-order',
    loadChildren: () =>
      import('./pages/limited-order/limited-order.module').then(
        (m) => m.LimitedOrderPageModule
      ),
  },
  {
    path: 'binanceconfig',
    loadChildren: () => import('./pages/configuration/binanceconfig/binanceconfig.module').then(m => m.BinanceconfigPageModule)
  },
  {
    path: 'tinkoffconfig',
    loadChildren: () => import('./pages/configuration/tinkoffconfig/tinkoffconfig.module').then(m => m.TinkoffconfigPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },

  {
    path: 'smart-screener',
    loadChildren: () => import('./pages/smart-screener/smart-screener.module').then( m => m.SmartScreenerPageModule)
  },
  {
    path: 'misc',
    loadChildren: () => import('./pages/configuration/tinkoffconfig/misc/misc.module').then( m => m.MiscPageModule)
  },
  {
    path: 'misc/:token',
    loadChildren: () => import('./pages/configuration/tinkoffconfig/misc/misc.module').then( m => m.MiscPageModule)
  },
  {
    path: 'orderlist',
    loadChildren: () => import('./pages/orderlist/orderlist.module').then( m => m.OrderlistPageModule)
  },
  {
    path: 'order-template',
    loadChildren: () => import('./pages/order-template/order-template.module').then( m => m.OrderTemplatePageModule)
  },
  {
    path: 'order-template/:ordertype/:orderid',
    loadChildren: () => import('./pages/order-template/order-template.module').then( m => m.OrderTemplatePageModule)
  },
  {
    path: 'screener',
    loadChildren: () => import('./pages/screener/screener.module').then( m => m.ScreenerPageModule)
  },
  {
    path: 'order-details',
    loadChildren: () => import('./pages/order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'portfolio-template',
    loadChildren: () => import('./pages/portfolio-template/portfolio-template.module').then( m => m.PortfolioTemplatePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
