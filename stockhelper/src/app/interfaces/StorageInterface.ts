export interface StorageInterface {
  getObject(id: string): any;
  setObject(id: string, object: any): void;
}
