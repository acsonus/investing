import { Status } from "../enumerations/Status";

export interface Message{
  status:Status;
  message:string;

}
