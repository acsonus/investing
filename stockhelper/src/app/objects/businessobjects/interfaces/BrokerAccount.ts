export interface BrokerAccount {
  token: string;
  name: string;
  type: string;

  getType(): string;
}
