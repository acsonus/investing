
import { TinkoffAccountType } from '../enumerations/TinkoffAccountType';

export class TinkoffAccount {
  brokerAccountId: string;
  brokerAccountType: TinkoffAccountType;
}
