import {TinkoffOrder} from './TinkoffOrder';


export class Bucket {
  executed: TinkoffOrder[];
  incomplete: TinkoffOrder[];

  constructor(executed: TinkoffOrder[], incomplete: TinkoffOrder[]) {
    this.executed = executed;
    this.incomplete = incomplete;
  }

}
