import {Bucket} from './TinkoffBucket';

export class TinkoffBuckets {
  longOrders: Bucket;
  shortOrders: Bucket;

  constructor(longOrders: Bucket, shortOrders: Bucket) {
    this.longOrders = longOrders;
    this.shortOrders = shortOrders;
  }
}
