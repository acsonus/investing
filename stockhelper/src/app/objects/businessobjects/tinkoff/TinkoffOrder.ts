export class TinkoffOrder {


  private _lots: number;
  private _price: number;
  private _status: string;
  private _rejectReason: string;
  private _operation: string;
  private _requestedLots: string;
  private _executedLots: number;
  private _type: string;
  private _figi:string;
  private _ticker:string
  private _id: string;


  constructor() {

  }


  get lots(): number {
    return this._lots;
  }

  get price(): number {
    return this._price;
  }

  get status(): string {
    return this._status;
  }

  get rejectReason(): string {
    return this._rejectReason;
  }

  get operation(): string {
    return this._operation;
  }

  get requestedLots(): string {
    return this._requestedLots;
  }

  get executedLots(): number {
    return this._executedLots;
  }

  get type(): string {
    return this._type;
  }

  get id(): string {
    return this._id;
  }


  get figi(): string {
    return this._figi;
  }

  set figi(value: string) {
    this._figi = value;
  }

  get ticker(): string {
    return this._ticker;
  }

  set ticker(value: string) {
    this._ticker = value;
  }
}
