import {TinkoffPayload} from './TinkoffPayload';
import {TinkoffError} from './TinkoffError';

export class TinkoffResponse {
  trackingId: string;
  status: string;
  payload: any;

  getMessage(): string {
    if (this.status === 'OK') {
      return 'Request is OK';
    }
    return 'An error occured';
  }

  public getPayLoad(): any {
    if ((!this.payload.code) && (!this.payload.message)) {
      return new TinkoffPayload(this.payload);
    } else {
      return new TinkoffError(this.payload.code, this.payload.message);
    }


  }

}
