export class ConnectionConfig {

  private constructor(apiUrl: string, socketUrl: string, token: string, currentAccount?: string, accounts?: string[]) {
    this.apiURL = apiUrl;
    this.socketURL = socketUrl;
    this.token = token;
    this.currentAccount = currentAccount;

  }

  private static instance: ConnectionConfig;
  private apiURL: string;
  private socketURL: string;
  private token: string;
  private currentAccount: string;
  private accounts: string[];


}
