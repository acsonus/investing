import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BinanceconfigPage } from './binanceconfig.page';

const routes: Routes = [
  {
    path: '',
    component: BinanceconfigPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BinanceconfigPageRoutingModule {}
