import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BinanceconfigPageRoutingModule } from './binanceconfig-routing.module';

import { BinanceconfigPage } from './binanceconfig.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BinanceconfigPageRoutingModule
  ],
  declarations: [BinanceconfigPage]
})
export class BinanceconfigPageModule {}
