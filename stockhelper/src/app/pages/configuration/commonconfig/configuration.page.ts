import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {
  binanceicon = '/assets/binance.svg';
  tinkofficon = '/assets/tinkoff.svg';

  constructor(private navCtrl: NavController) {}

  ngOnInit(): void {}

  navTinkoff() {
    this.navCtrl.navigateForward('tinkoffconfig');
  }

  navBinance() {
    this.navCtrl.navigateForward('binanceconfig');
  }
}
