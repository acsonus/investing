import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'misc'
})
export class MiscPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return null;
  }

}
