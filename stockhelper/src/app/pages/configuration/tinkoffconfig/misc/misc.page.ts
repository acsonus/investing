import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TinkoffAccountType } from 'src/app/objects/businessobjects/enumerations/TinkoffAccountType';
import { TinkoffAccount } from 'src/app/objects/businessobjects/tinkoff/TinkoffAccount';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { LocalStorageService } from 'src/app/services/storage/localstorage.service';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';

@Component({
  selector: 'app-misc',
  templateUrl: './misc.page.html',
  styleUrls: ['./misc.page.scss'],
})
export class MiscPage implements OnInit {
  token: string;
  tinkoffAccounts: TinkoffAccount[];
  selectedAccount: TinkoffAccount;
  selectedAccountId:string;
  showDetails = false;
  updateIntervals: string[] = ['none', '5m', '10m', '15m', '30m', '1h'];
  selectedUpdateInterval: string;
  useSandbox:boolean = false;
  constructor(
    private configurationService: ConfigurationService,
    private tinkoffFacadeService: TinkoffacadeService,
    private activatedRoute: ActivatedRoute,
    private lss: LocalStorageService
  ) {}

  ngOnInit() {}
  async ionViewWillEnter() {
    console.log('miscellaneous view entered');
    //loading configuration from services
    //get token
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('token')) {
        //redirect
        return;
      }
      this.token = paramMap.get('token');
    });
    //console.log('token=' + this.token);

    if (this.token) {
      // order here is important
      
      let sandbox:string = await this.configurationService.getTinkoffUseSandbox();
      this.useSandbox = JSON.parse(sandbox);
      console.log(this.useSandbox);
      this.selectedUpdateInterval  = await this.configurationService.getTinkoffUpdateInterval()
      await this.loadBrokerAccounts(this.token,this.useSandbox);
      this.loadSelectedAccount();
    }

  }

  public accountChanged() {

    console.log('selected account '+ this.selectedAccount.brokerAccountId);
    this.chooseAccount(this.selectedAccount.brokerAccountType);
  }

  private chooseAccount(accountType: TinkoffAccountType) {
    console.log(this.tinkoffAccounts);
    this.selectedAccount = this.tinkoffAccounts.filter(
      (value) => {return (value.brokerAccountType === accountType)}
    )[0];
    console.log(this.selectedAccount);
    this.configurationService.setTinkoffCurrentAccountId(this.selectedAccount);
  }

  private async loadSelectedAccount() {
    const accountId =
      await this.configurationService.getTinkoffCurrentAccountId();
    this.selectedAccount = this.tinkoffAccounts.filter(
      (value) => value.brokerAccountId === accountId
    )[0];
  }

  public updateIntervalChanged() {
    console.log('update Interval changed');
    this.configurationService.setTinkoffUpdateInterval(
      this.selectedUpdateInterval
    );
  }
  private async loadBrokerAccounts(token:string, sandbox: boolean) {
    this.tinkoffAccounts = await this.tinkoffFacadeService.getUserAccounts(token,sandbox);

  }

}
