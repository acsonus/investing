import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiscPage } from './misc/misc.page';

import { TinkoffconfigPage } from './tinkoffconfig.page';

const routes: Routes = [
  {
    path: '',
    component: TinkoffconfigPage
  },
  {
    path: 'misc',
    loadChildren: () => import('./misc/misc.module').then( m => m.MiscPageModule)
  },
  {
    path: 'misc/:token',
    component:MiscPage
    //loadChildren: () => import('./misc/misc.module').then( m => m.MiscPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TinkoffconfigPageRoutingModule {}
