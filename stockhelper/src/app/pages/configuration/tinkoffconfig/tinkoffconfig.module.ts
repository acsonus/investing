import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TinkoffconfigPageRoutingModule } from './tinkoffconfig-routing.module';

import { TinkoffconfigPage } from './tinkoffconfig.page';
import { MiscPipe } from './misc.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TinkoffconfigPageRoutingModule
  ],
  declarations: [TinkoffconfigPage]
})
export class TinkoffconfigPageModule {}
