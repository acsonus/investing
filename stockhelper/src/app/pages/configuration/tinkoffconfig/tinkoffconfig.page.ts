import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Status } from 'src/app/objects/businessobjects/enumerations/Status';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';
import { ConfigurationService } from '../../../services/configuration/configuration.service';

@Component({
  selector: 'app-tinkoffconfig',
  templateUrl: './tinkoffconfig.page.html',
  styleUrls: ['./tinkoffconfig.page.scss'],
})
export class TinkoffconfigPage {
  token: string;
  tokenRepeat: string;
  private selectedAccountType: string;
  useSandbox:boolean = false;
  miscActive: boolean;

  constructor(
    private tinkoffFacedeService: TinkoffacadeService,
    private alertCtrl: AlertController,
    private configService: ConfigurationService
  ) {}

  async ionViewWillEnter() {
    console.log('view will enter called');
    const secretToken: string =
      await this.configService.getTinkoffSecretToken();
    if (secretToken && secretToken !== '') {
      this.token = this.tokenRepeat = secretToken;
      const sandbox = await this.configService.getTinkoffUseSandbox();
      this.useSandbox =JSON.parse(sandbox);
;
      const tokenIsValid = await this.validateToken(false);
     
      this.miscActive = tokenIsValid;
    }
  }
  public async validateToken(showDialog: boolean) {
    if (this.token !== this.tokenRepeat) {
      const dlg = await this.alertCtrl.create({
        header: 'Error ',
        subHeader: 'Entered tokens do not match',
        buttons: ['Dismiss'],
      });
      await dlg.present();
      return false;
    }
    if (this.token) {
      const useSandbox: boolean = JSON.parse(
        await this.configService.getTinkoffUseSandbox()
      );
      console.log("Using sandbox="+this.useSandbox);
      const result = await this.tinkoffFacedeService.tinkoffValidateToken(
        this.token,
        this.useSandbox
      );

      const dlg = await this.alertCtrl.create({
        header: result.status === Status.Ok ? 'Success' : 'Error',
        subHeader: result.message,
        buttons: ['Dismiss'],
      });
      if (showDialog) await dlg.present();
      return true;
    } else {
      const dlg = await this.alertCtrl.create({
        header: 'Warning',
        subHeader: 'Please enter token to proceed',
        buttons: ['Dismiss'],
      });
      await dlg.present();
      return false;
    }
  }

  public async  useSandboxClicked(){
    console.log(this.useSandbox);
    await this.configService.setTinkoffUseSandbox(this.useSandbox);
  }
  public async saveToken() {
    if (this.useSandbox===undefined){
      this.useSandbox = false;
    }
    await this.configService.setTinkoffUseSandbox(this.useSandbox);
    if (await this.validateToken(false)) {
      const dlg = await this.alertCtrl.create({
        header: 'Success',
        subHeader: 'Token is successfully saved',
        buttons: ['Dismiss'],
      });
      await dlg.present();
      await this.configService.setTinkoffSecretToken(this.token);
      
      this.miscActive = true;
    } else {
      this.miscActive = false;
    }
  }

}
