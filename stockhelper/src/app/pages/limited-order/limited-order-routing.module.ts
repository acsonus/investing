import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LimitedOrderPage } from './limited-order.page';

const routes: Routes = [
  {
    path: '',
    component: LimitedOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LimitedOrderPageRoutingModule {}
