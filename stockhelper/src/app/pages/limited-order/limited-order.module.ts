import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LimitedOrderPageRoutingModule } from './limited-order-routing.module';

import { LimitedOrderPage } from './limited-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LimitedOrderPageRoutingModule
  ],
  declarations: [LimitedOrderPage]
})
export class LimitedOrderPageModule {}
