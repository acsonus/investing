import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TinkoffWebServicesService } from 'src/app/services/tinkoff/tinkoffWebservices.service';
import { MarketInstrument } from '../../apis/tinkoffapi1.3/domain';
import { TinkoffAccount } from '../../objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffOrder } from '../../objects/businessobjects/tinkoff/TinkoffOrder';
import { ConfigurationService } from '../../services/configuration/configuration.service';
import { FinancialService } from '../../services/financial/financial.service';

@Component({
  selector: 'app-limited-order',
  templateUrl: './limited-order.page.html',
  styleUrls: ['./limited-order.page.scss'],
})
export class LimitedOrderPage implements OnInit {

  selectedInstrument: string;
  listOfSelectedMarketInstruments: MarketInstrument[]
  filteredList: MarketInstrument[];
  listOrdersToDisplay: TinkoffOrder[];
  brokerAccounts: TinkoffAccount[];
  currentAccount: TinkoffAccount;
  newTicker: string;
  newOperation: string;
  newLotsNumber: number;
  newPrice: number;
  intervalId;


  constructor() {
  }

  async ngOnInit() {
  }

  /**
   * lifecycle events
   */

  filterList(event: any) {

    const searchstring = event.srcElement.value;
    if (searchstring === '') {
      this.filteredList = this.listOfSelectedMarketInstruments;
    }
    console.log(this.listOfSelectedMarketInstruments);
    this.filteredList = this.listOfSelectedMarketInstruments.filter((mi) => { 

        return (mi.name.indexOf(searchstring)>=0)


    });
  }
  instrumentsChanged(){

  }
  async ionViewDidEnter() {
    console.log('entered limited order view');


  }



}
