import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderTemplatePageRoutingModule } from './order-template-routing.module';

import { OrderTemplatePage } from './order-template.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderTemplatePageRoutingModule
  ],
  declarations: [OrderTemplatePage]
})
export class OrderTemplatePageModule {}
