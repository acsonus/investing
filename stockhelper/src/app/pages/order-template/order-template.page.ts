import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LimitOrderResponse } from 'src/app/apis/tinkoffapi1.3/domain';
import { TinkoffAccount } from 'src/app/objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffOperation } from 'src/app/objects/businessobjects/tinkoff/TinkoffOperation';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';

@Component({
  selector: 'app-order-template',
  templateUrl: './order-template.page.html',
  styleUrls: ['./order-template.page.scss'],
})
export class OrderTemplatePage implements OnInit {
  token: string;
  currentAccountId: string;
  title: string;
  ticker: string;
  operation: string
  price: string;
  useSandbox: boolean;
  lots: string;
  constructor(private tinkoffFacade: TinkoffacadeService,
    private configService: ConfigurationService,
    private alertCtrl: AlertController) { }

  async ngOnInit() {
    this.title = "New order";
    this.token = await this.configService.getTinkoffSecretToken();
    this.currentAccountId = await this.configService.getTinkoffCurrentAccountId();
    this.useSandbox = JSON.parse(await this.configService.getTinkoffUseSandbox());
  }

  async placeOrder() {

    if (Number(this.lots) > 0 && Number(this.price) > 0 && this.operation) {

      console.log(this.operation + " " + this.lots + " of " + this.ticker + "  for " + Number(this.lots) * Number(this.price));
      let figi = await this.tinkoffFacade.getFigiByTicker(this.ticker,
        await this.configService.getTinkoffSecretToken(),
        JSON.parse(await this.configService.getTinkoffUseSandbox()));
      let tinkoffOperation = { figi, lots: Number(this.lots), operation: this.operation, price: Number(this.price) };
      let currentAccountId = await this.configService.getTinkoffCurrentAccountId();
      let currentBrokerAccount = await this.getAccountById(currentAccountId);

      if (this.useSandbox) {
        await this.tinkoffFacade.setSandboxBalance(currentAccountId, "USD", Number(this.lots) * Number(this.price) + 100, this.token);
      }
      let limitOrderResp: LimitOrderResponse = await this.tinkoffFacade.setTinkoffLimitedOrder(tinkoffOperation, currentBrokerAccount, this.token, this.useSandbox);
      let pl = limitOrderResp.payload;
      if (limitOrderResp.status === "Ok") {

        const dlg = await this.alertCtrl.create({
          header: 'Success',
          subHeader: pl.message,
          buttons: ['Dismiss'],
        });
        await dlg.present();
      }
      // if (limitOrderResp.status === "Error") {
      //   const dlg = await this.alertCtrl.create({
      //     header: 'Error',
      //     subHeader: pl.message,
      //     buttons: ['Dismiss'],
      //   });
      //   await dlg.present();
      // }
      console.log("Order " + pl.orderId + "placed with status" + pl.status + ". Executed  " + pl.executedLots + " of " + pl.requestedLots);
    }

  }





  async getAccountById(brokerAccountId: string): Promise<TinkoffAccount> {
    let result: TinkoffAccount = null;
    let tas: TinkoffAccount[] = await this.tinkoffFacade.getUserAccounts(this.token, this.useSandbox)
    for (let ta of tas) {
      if (ta.brokerAccountId === ta.brokerAccountId) {
        return result = ta;
      }
    }
    return result;
  }

}
