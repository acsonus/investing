import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/apis/tinkoffapi1.3/domain';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.page.html',
  styleUrls: ['./orderlist.page.scss'],
})
export class OrderlistPage implements OnInit {
  orderList:Order[];
  token:string;
  selectedAccount: string;
  useSandbox:boolean;
  constructor(private tinkoffFacadeService: TinkoffacadeService, private configurationService: ConfigurationService) { }



  async ngOnInit() {
    this.token = await this.configurationService.getTinkoffSecretToken();
  }
  
  async ionViewWillEnter() {
    this.selectedAccount = await this.configurationService.getTinkoffCurrentAccountId();
    this.useSandbox= JSON.parse(await this.configurationService.getTinkoffUseSandbox());
    this.orderList = await this.tinkoffFacadeService.getTinkoffActiveOrders(this.token, this.selectedAccount, this.useSandbox);
      
     for (let order  of this.orderList){
       order.ticker = ( await this.tinkoffFacadeService.getTickerByFigi(order.figi, this.token,this.useSandbox)).payload.ticker;
     }
     for (let order  of this.orderList){
       console.log('List contains '+ order.ticker);
    }
  }
}