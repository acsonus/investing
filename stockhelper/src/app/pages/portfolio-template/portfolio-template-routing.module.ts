import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortfolioTemplatePage } from './portfolio-template.page';

const routes: Routes = [
  {
    path: '',
    component: PortfolioTemplatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PortfolioTemplatePageRoutingModule {}
