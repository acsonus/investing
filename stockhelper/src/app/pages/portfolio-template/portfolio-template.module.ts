import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PortfolioTemplatePageRoutingModule } from './portfolio-template-routing.module';

import { PortfolioTemplatePage } from './portfolio-template.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PortfolioTemplatePageRoutingModule
  ],
  declarations: [PortfolioTemplatePage]
})
export class PortfolioTemplatePageModule {}
