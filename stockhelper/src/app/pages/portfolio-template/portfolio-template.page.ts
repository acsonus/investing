import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { CurrencyPosition, Portfolio, PortfolioCurrenciesResponse, PortfolioPosition, PortfolioResponse } from 'src/app/apis/tinkoffapi1.3/domain';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';

@Component({
  selector: 'app-portfolio-template',
  templateUrl: './portfolio-template.page.html',
  styleUrls: ['./portfolio-template.page.scss'],
})
export class PortfolioTemplatePage implements OnInit {
  title: string;
  token: string;
  currentAccountId: string;
  useSandbox: boolean;
  portFolio: PortfolioResponse;
  portfolioCurrenciesResponse:PortfolioCurrenciesResponse
  assets: PortfolioPosition[];
  currencies: CurrencyPosition[];
  constructor(private configurationService: ConfigurationService,
    private tinkoffFacade: TinkoffacadeService) {

  }
  async eraseAllStockItems(){
    await this.tinkoffFacade.clearSandboxAccount(this.currentAccountId,this.token);
    await this.loadData();
  }

  async ionViewWillEnter() {
    this.title = "Current portfolio";
    this.token = await this.configurationService.getTinkoffSecretToken();
    this.useSandbox = JSON.parse(await this.configurationService.getTinkoffUseSandbox());
    this.currentAccountId = await this.configurationService.getTinkoffCurrentAccountId();
    await this.loadData();
  }
  async loadData(){
    if (this.useSandbox){
      await this.tinkoffFacade.registerSandbox(this.token);
    }
    this.portFolio = await this.tinkoffFacade.getPortfolio(this.currentAccountId, this.token, this.useSandbox);
    if (this.portFolio.payload.positions){
      this.assets = this.portFolio.payload.positions;
      console.log(this.assets);
    }else {
      console.log("There are no assets in portfolio");
    }
    this.portfolioCurrenciesResponse = await this.tinkoffFacade.getCurrencies(this.currentAccountId, this.token, this.useSandbox)
    if (this.portfolioCurrenciesResponse.payload.currencies){
      this.currencies = this.portfolioCurrenciesResponse.payload.currencies;
      console.log(this.currencies);
    }else {
      console.log("There are no currencies in portfolio");
    }

  }
 async  ngOnInit() {

  }

}
