import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { MarketInstrument } from 'src/app/apis/tinkoffapi1.3/domain';
import { TinkoffAccount } from 'src/app/objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffOrder } from 'src/app/objects/businessobjects/tinkoff/TinkoffOrder';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { FinancialService } from 'src/app/services/financial/financial.service';
import { TinkoffWebServicesService } from 'src/app/services/tinkoff/tinkoffWebservices.service';

@Component({
  selector: 'app-screener',
  templateUrl: './screener.page.html',
  styleUrls: ['./screener.page.scss'],
})
export class ScreenerPage implements OnInit {

  

  constructor(private fs: FinancialService, private cs: ConfigurationService, private alertCtrl: AlertController, tinkoffService: TinkoffWebServicesService) { }

  ngOnInit() {
  }
  
}
