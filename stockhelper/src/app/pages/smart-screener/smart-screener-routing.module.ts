import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SmartScreenerPage } from './smart-screener.page';

const routes: Routes = [
  {
    path: '',
    component: SmartScreenerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmartScreenerPageRoutingModule {}
