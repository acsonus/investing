import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SmartScreenerPageRoutingModule } from './smart-screener-routing.module';

import { SmartScreenerPage } from './smart-screener.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SmartScreenerPageRoutingModule
  ],
  declarations: [SmartScreenerPage]
})
export class SmartScreenerPageModule {}
