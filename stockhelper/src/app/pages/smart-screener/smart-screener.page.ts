import { Component, OnInit } from '@angular/core';
import { Candle, CandlesResponse, MarketInstrument, MarketInstrumentList } from 'src/app/apis/tinkoffapi1.3/domain';
import { TinkoffAccount } from 'src/app/objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffOrder } from 'src/app/objects/businessobjects/tinkoff/TinkoffOrder';
import { ConfigurationService } from 'src/app/services/configuration/configuration.service';
import { TinkoffacadeService } from 'src/app/services/tinkoff/tinkoffacade.service';

@Component({
  selector: 'app-smart-screener',
  templateUrl: './smart-screener.page.html',
  styleUrls: ['./smart-screener.page.scss'],
})
export class SmartScreenerPage implements OnInit {
  selectedInstrument: string;
  listOfSelectedMarketInstruments;
  filteredList;
  listOrdersToDisplay: TinkoffOrder[];
  brokerAccounts: TinkoffAccount[];
  currentAccount: TinkoffAccount;
  newTicker: string;
  newOperation: string;
  newLotsNumber: number;
  newPrice: number;
  intervalId;




  //default settigs from configuration
  token: string;
  useSandbox: boolean;
  currentBrockerAccountId: string;
  updateInterval: string;
  //ajustable setting from the form
  showSettings: boolean;
  currentBehaviour: string;
  currentPercentage: number;
  currentTimeFrame: string;
  miElement: Object[];
  progress:number=0;
  searching = false;
  constructor(private configService: ConfigurationService, private tinkoffFacade: TinkoffacadeService) { }

  async ngOnInit() {
    this.token = await this.configService.getTinkoffSecretToken();
    this.currentBrockerAccountId = await this.configService.getTinkoffCurrentAccountId();
    this.updateInterval = await this.configService.getTinkoffUpdateInterval();
    this.useSandbox = JSON.parse(await this.configService.getTinkoffUseSandbox());
    this.filteredList=[];
    this.showSettings = false;
  }
  public toggleSettings() {
    if (this.showSettings) {
      this.showSettings = false;
    } else {
      this.showSettings = true;
    }
  }
  filterList(event: any) {

    // const searchstring = event.srcElement.value;
    // if (searchstring === '') {
    //   this.filteredList = this.listOfSelectedMarketInstruments;
    // }
    // console.log(this.listOfSelectedMarketInstruments);
    // this.filteredList = this.listOfSelectedMarketInstruments.filter((mi) => {

    //   return (mi.name.indexOf(searchstring) >= 0)
    // });
  }
  async instrumentsChanged() {
    console.log('Instruments Changed');


    this.listOfSelectedMarketInstruments = null;

    switch (this.selectedInstrument) {
      case 'bonds': { this.listOfSelectedMarketInstruments = (await this.tinkoffFacade.retrieveAllBonds(this.token, this.useSandbox)); break; }
      case 'stocks': { this.listOfSelectedMarketInstruments = (await this.tinkoffFacade.retrieveAllStocks(this.token, this.useSandbox)); break; }
      case 'etfs': { this.listOfSelectedMarketInstruments = (await this.tinkoffFacade.retrieveAllEtfs(this.token, this.useSandbox)); break; }
      case 'currencies': { this.listOfSelectedMarketInstruments = (await this.tinkoffFacade.retrieveAllCurrencies(this.token, this.useSandbox)); break; }
      default: { console.log('Nothing selected return'); return; }
    }


  }
  async ionViewDidEnter() {
    console.log('entered limited order view');
    
  }
  async search() {
    await this.growFallRecognize(this.currentBehaviour, this.currentPercentage, this.currentTimeFrame)
  }
  stop(){
    this.searching = false;
    this.progress=1;
  }


  /**
   * 
   * @param behaviour  this defines what behaviour is expected bull/bear market
   * @param percentage for how many percent the price has raised/fallen
   * @param timeframe  timeframe taken for conclusion 
   */
  async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  async growFallRecognize(behaviour: string, percentage: number, timeframe: string) {
    if (percentage > 0 && timeframe) {

      const tzoffset = (new Date()).getTimezoneOffset() * 60000;
      let now = new Date(Date.now()-tzoffset);
      //now.setHours(now.getHours() + 3)
      let someTimeBeforeNow = new Date(Date.now()-tzoffset);
      now.setMinutes(now.getMinutes()-1)
      //someTimeBeforeNow.setHours(someTimeBeforeNow.getHours() + 3);
      someTimeBeforeNow.setMinutes(someTimeBeforeNow.getMinutes()-1 - Number(timeframe.slice(0, timeframe.indexOf('m'))));
      const nowISO = now.toISOString();//.slice(0, 19);.replace('T', ' ');;
      const someTimeBeforeNowISO = someTimeBeforeNow.toISOString();//.slice(0, 19).replace('T', ' ');;
      const endInterval = nowISO;//.slice(0,nowISO.length-5)+'+3:00';
      const startInterval = someTimeBeforeNowISO;//.slice(0,someTimeBeforeNowISO.length-5)+'+3:00';
      console.log(startInterval);
      console.log(endInterval);
      console.log('start processing instruments');
      //bull bear market coeff
      let counter:number =0;
      this.searching = true;
      this.progress=0;
      let interval = "1min";
      const numberofMI = this.listOfSelectedMarketInstruments.instruments[0].length;
      for (let mi of this.listOfSelectedMarketInstruments.instruments[0]) {
        //console.log('Canldles for ' + (mi as MarketInstrument).ticker);
        const resolutionNumber = Number(timeframe.slice(0, timeframe.indexOf('m')));
        if (resolutionNumber>35){
          interval = '10min';
        } else if (resolutionNumber>15){
          interval='5min';
        }
        let candles: Candle[] = await this.retrieveMiObjects((mi as MarketInstrument).figi, startInterval, endInterval,interval);
        this.progress =counter/numberofMI;
        console.log( "Progress:"+this.progress)
        if (candles.length > 0) {
          console.log("there are candles inside");

          let change = (candles[candles.length - 1].c - candles[0].c) / ((candles[candles.length - 1].c + candles[0].c) / 2);
          if (Math.abs(change)>=Math.abs( percentage / 100)) {
            let msg: string = change>0? "Strong buy of " : " Strong sell of ";
            mi.message = msg;
            this.filteredList.push(mi);
            console.log(msg + (mi as MarketInstrument).name + " as " + (mi as MarketInstrument).ticker);
          }
          await this.delay(2000);
          // for (let candle of candles){ 


          //   console.log( candle.figi+ " at "+candle.time+" closed "+ candle.c);

          // }
        }
        else{
          await this.delay(200);
        } 
        counter++;
        if (!this.searching){
          break;
        }
  
      }
      //this.retrieveMiObjects('fasdfkaf', 'Sell', 1, '5min');
      console.log('end processing instruments');
      
    }
  }
  async retrieveMiObjects(figi: string, startInterval: string, endInterval: string, interval:string): Promise<Candle[]> {
    //console.log( 'Candles for '+figi);

    const candlesResponse: CandlesResponse = await this.tinkoffFacade.getCandles('BBG000B9XRY4', startInterval, endInterval, interval, this.token, this.useSandbox);
    const candles: Candle[] = candlesResponse.payload.candles;
    return candles;
    // if ()
    // if (candles[0].c)
    // candles[]

  }

}
