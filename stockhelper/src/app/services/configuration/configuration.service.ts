import {Injectable} from '@angular/core';
import {LocalStorageService} from '../storage/localstorage.service';
import {TinkoffAccount} from '../../objects/businessobjects/tinkoff/TinkoffAccount';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {


  constructor(private ls: LocalStorageService) {
  }

// if nothing is passed within this range not updated
  public async  setTinkoffUpdateInterval(updatePeriod: string) {

    await this.ls.setObject('tinkoff.updateInterval', updatePeriod);
  }

  public async getTinkoffUpdateInterval(): Promise<string> {
    return await this.ls.getObject('tinkoff.updateInterval');
  }

  public setTinkoffCurrentAccountId(tinkoffAccount: TinkoffAccount) {
    this.ls.setObject('tinkoff.currentAccountId', tinkoffAccount.brokerAccountId);
  }

  private getPeriodInMilliseconds(seconds: number): number {
    return seconds * 60 * 1000;
  }

  public async getTinkoffCurrentAccountId(): Promise<string> {

    return await this.ls.getObject('tinkoff.currentAccountId');
  }

  public async  getTinkoffSecretToken() {
    return  await this.ls.getObject('tinkoff.secretToken');
  }

  public  async setTinkoffSecretToken(token: string) {
    await this.ls.setObject('tinkoff.secretToken', token);
  }

  public   setTinkoffUseSandbox(value:boolean){
     this.ls.setObject('tinkoff.useSandbox', String(value));
  }
  public  getTinkoffUseSandbox() :Promise<string>{
    return  this.ls.getObject('tinkoff.useSandbox');
    
  }
}
