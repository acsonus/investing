import {TinkoffWebServicesService} from '../tinkoff/tinkoffWebservices.service';
import {LocalStorageService} from '../storage/localstorage.service';

/**
 *
 * o цена открытия
 * c цена закрытия
 * h наибольшая цена
 * l наименьшая цена
 * v объем торгов
 */
import {Injectable} from '@angular/core';
import {BinanceWebServices} from '../binance/binanceWebservices.service';
import {TinkoffAccount} from '../../objects/businessobjects/tinkoff/TinkoffAccount';
import {TinkoffOperation} from '../../objects/businessobjects/tinkoff/TinkoffOperation';
import { Message } from 'src/app/objects/businessobjects/common/Message';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {


  constructor(private localStorageService: LocalStorageService,
              private tinkoffWebServices: TinkoffWebServicesService,
              private binanceWebServices: BinanceWebServices,
              localStorageservice: LocalStorageService
  ) {

  }

}
