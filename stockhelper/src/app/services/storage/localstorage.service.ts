import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';
import {StorageInterface} from '../../interfaces/StorageInterface';

const {Storage} = Plugins;

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService implements StorageInterface {
  constructor() {
  }

  public async setObject(key: string, value: string) {
    await Storage.set({key, value});
  }

  public async getObject(key: string): Promise<string> {
    return (await Storage.get({key})).value;
  }

  public processToken(tokenObject: any): string {
    if (tokenObject instanceof String) {
      return tokenObject as string;

    } else {
      return undefined;
    }
  }
}
