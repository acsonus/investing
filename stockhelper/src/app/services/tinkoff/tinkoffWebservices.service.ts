import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PortfolioCurrenciesResponse, PortfolioResponse } from 'src/app/apis/tinkoffapi1.3/domain';
import { Message } from 'src/app/objects/businessobjects/common/Message';
import { Status } from 'src/app/objects/businessobjects/enumerations/Status';
import { TinkoffAccount } from '../../objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffOperation } from '../../objects/businessobjects/tinkoff/TinkoffOperation';

@Injectable({
  providedIn: 'root',
})

// seems like to be a strategy
export class TinkoffWebServicesService {
  private sandboxLink: string;
  private tradingLink: string;
  private websocketLink: string;
  private sandBoxRegistered: boolean;

  constructor(private client: HttpClient) {
    this.tradingLink = 'https://api-invest.tinkoff.ru/openapi';
    this.sandboxLink = 'https://api-invest.tinkoff.ru/openapi/sandbox';
    this.websocketLink = 'wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws';
    this.sandBoxRegistered = false;
  }
  public getSocket(token:string){



  }

  public getHeaders(token: string): HttpHeaders {
    return new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + token);
  }
  getLink(sandbox: boolean): string {
    return (sandbox) ? this.sandboxLink : this.tradingLink;
  }
  public async getAccounts(token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    return this.client
      .get(this.getLink(sandbox) + '/user/accounts', {
        headers,
      })
      .toPromise();
  }

  public async getActiveOrders(token: string, accountId: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    return this.client
      .get(this.getLink(sandbox) + '/orders', {
        headers,
      })
      .toPromise();
  }

  public async limitOrder(
    tinkoffOperation: TinkoffOperation,
    account: TinkoffAccount,
    token: string,
    sandbox: boolean
  ): Promise<any> {
    const headers = this.getHeaders(token);
    const params = new HttpParams()
      .set('figi', tinkoffOperation.figi)
      .set('brokerAccountId', account.brokerAccountId);
    const body = {
      lots: tinkoffOperation.lots,
      operation: tinkoffOperation.operation,
      price: tinkoffOperation.price,
    };


   let result = null ;

   try {
    result = await this.client
      .post(this.getLink(sandbox) + '/orders/limit-order', body, {
        headers,
        params,
      })
      .toPromise();
    } catch(e){
      result = new Promise( (resolve,reject)=>{reject(e)});

    }
    return result;
  }

  public async getFigiByTicker(ticker: string, token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('ticker', ticker);
    return this.client
      .get(this.getLink(sandbox) + '/market/search/by-ticker', {
        headers,
        params,
      })
      .toPromise();
  }

  public async getAllStocks(token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    const response: any = await this.client
      .get(this.getLink(sandbox) + '/market/stocks', {
        headers,
      })
      .toPromise();
    return response;
  }
  public async getAllBonds(token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    const response: any = await this.client
      .get(this.getLink(sandbox) + '/market/bonds', {
        headers,
      })
      .toPromise();
    return response;
  }
  public async getAllETFs(token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    const response: any = await this.client
      .get(this.getLink(sandbox) + '/market/etfs', {
        headers,
      })
      .toPromise();
    return response;
  }
  public async getAllCurrencies(token: string, sandbox: boolean): Promise<any> {
    const headers = this.getHeaders(token);
    const response: any = await this.client
      .get(this.getLink(sandbox) + '/market/etfs', {
        headers,
      })
      .toPromise();
    return response;
  }

  public async getUserAccounts(token: string, sandbox: boolean): Promise<any> {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + token);
    return this.client
      .get(this.getLink(sandbox) + '/user/accounts', {
        headers,
      })
      .toPromise();
  }

  public async cancelOrder(
    orderId: string,
    brokerAccountId: string,
    token: string,
    sandbox: boolean
  ): Promise<any> {
    const headers = this.getHeaders(token);
    const params = new HttpParams()
      .set('orderId', orderId)
      .set('brokerAccountId', brokerAccountId);
    return this.client
      .post(this.getLink(sandbox) + '/orders/cancel', null, {
        headers,
        params,
      })
      .toPromise();
  }


  public async getTickerByFigi(
    figi: string,
    token: string,
    sandbox: boolean
  ): Promise<any> {

    const headers = this.getHeaders(token);
    const params = new HttpParams().set('figi', figi);
    return this.client
      .get(this.getLink(sandbox) + '/market/search/by-figi', {
        headers,
        params,
      })
      .toPromise();
  }

  public async validateToken(token: string, sandbox: boolean): Promise<Message> {
    let response: Message = null;
    try {
      const resp = await this.getAccounts(token, sandbox);

      if (resp.payload.accounts) {
        response = { status: Status.Ok, message: "Validation sucessfull" };
      } else {
        response = { status: Status.Nok, message: "No broker accounts found" };
      }
    } catch (e) {
      response = { status: Status.Nok, message: "Token is incorrect" };
    }
    return response;
  }


  public async registerSandbox(token: string) {
    this.sandBoxRegistered = true;
    const headers = this.getHeaders(token);
    return this.client.post(this.sandboxLink + "/sandbox/register", { "brokerAccountType": "Tinkoff" }, { headers }).toPromise();

  }
  // clear all positions on a sandbox
  public async clearSandbox(brockerAccountId: string, token: string) {
    this.sandBoxRegistered = false;
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('brokerAccountId', brockerAccountId);
    return this.client.post(this.sandboxLink + "/sandbox/clear", null, { headers, params }).toPromise();

  }

  public async getCandles(figi:string, fromtimestamp:string, totimestamp:string, interval:string, token:string, sandbox: boolean){
    const headers = this.getHeaders(token);
    const params = new HttpParams().
    set('figi', figi).
    set('from', fromtimestamp).
    set('to', totimestamp).
    set('interval', interval);
   return  this.client
    .get(this.getLink(sandbox) + '/market/candles', {
      headers,
      params,
    })
    .toPromise();

  }



  public async setSandboxBalace(brockerAccountId: string, currency: string, balance: number, token: string) {
    this.sandBoxRegistered = false;
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('brokerAccountId', brockerAccountId);
    return this.client.post(this.sandboxLink + "/sandbox/currencies/balance", { currency, balance }, { headers, params }).toPromise();

  }
  //delete brocker account on a sandbox
  public async removeSandboxAccount(brockerAccountId: string, token: string) {
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('brokerAccountId', brockerAccountId);
    return this.client.post(this.sandboxLink + "/sandbox/remove", null, { headers, params }).toPromise();

  }

  public async getPortfolio(brockerAccountId: string, token: string, sandbox: boolean):Promise<PortfolioResponse> {
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('brokerAccountId', brockerAccountId);
    return this.client.get(this.getLink(sandbox) + "/portfolio", { headers, params }).toPromise() as Promise<PortfolioResponse>;

  }
  public async getCurrencies(brockerAccountId: string, token: string, sandbox: boolean): Promise<PortfolioCurrenciesResponse> {
    const headers = this.getHeaders(token);
    const params = new HttpParams().set('brokerAccountId', brockerAccountId);
    return this.client.get(this.getLink(sandbox) + "/portfolio/currencies", { headers, params }).toPromise() as Promise<PortfolioCurrenciesResponse> ;
  }

  // config.apiURL = 'https://api-invest.tinkoff.ru/openapi';
  // config.socketURL =
  //   'wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws';
  // config.token = token;l

}
