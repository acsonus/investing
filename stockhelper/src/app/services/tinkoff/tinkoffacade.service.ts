import { Injectable } from '@angular/core';
import {
  CandlesResponse,
  MarketInstrumentList, PortfolioCurrenciesResponse
} from 'src/app/apis/tinkoffapi1.3/domain';
import { Message } from 'src/app/objects/businessobjects/common/Message';
import { TinkoffAccountType } from 'src/app/objects/businessobjects/enumerations/TinkoffAccountType';
import { TinkoffOperation } from 'src/app/objects/businessobjects/tinkoff/TinkoffOperation';
import { TinkoffAccount } from '../../objects/businessobjects/tinkoff/TinkoffAccount';
import { TinkoffWebServicesService } from './tinkoffWebservices.service';

@Injectable({
  providedIn: 'root',
})

// this services are for abstraction life scenarios from api requests
export class TinkoffacadeService {
  constructor(private tinkoffWebServices: TinkoffWebServicesService) { }

  public async getUserAccounts(
    token: string,
    sandbox: boolean
  ): Promise<TinkoffAccount[]> {
    let tinkoffAccounts: TinkoffAccount[] = [];
    let accountsMessage = await this.tinkoffWebServices.getAccounts(
      token,
      sandbox
    );
    if (accountsMessage.payload.accounts) {
      let srcAccounts = accountsMessage.payload.accounts;
      for (let account of srcAccounts) {
        let acc = new TinkoffAccount();
        acc.brokerAccountId = account.brokerAccountId;
        let type = null;
        switch (account.brokerAccountType) {
          case 'Tinkoff': {
            type = TinkoffAccountType.Broker;
            break;
          }
          case 'TinkoffIis': {
            type = TinkoffAccountType.IIS;
            break;
          }
          default: {
            type = null;
          }
        }
        acc.brokerAccountType = type;
        tinkoffAccounts.push(acc);
      }
    }
    console.log(tinkoffAccounts);
    return tinkoffAccounts;
  }
  ///tinkoff financial services

  public async setTinkoffLimitedOrder(
    operation: TinkoffOperation,
    ta: TinkoffAccount,
    token: string,
    sandbox: boolean
  ) {
    return this.tinkoffWebServices.limitOrder(operation, ta, token, sandbox);
  }

  public async tinkoffValidateToken(
    token: string,
    sandbox: boolean
  ): Promise<Message> {
    return this.tinkoffWebServices.validateToken(token, sandbox);
  }
  public async retrieveAllBonds(
    token: string,
    sandbox: boolean
  ): Promise<MarketInstrumentList> {
    let milList: MarketInstrumentList = { total: 0, instruments: [] };
    //read all bonds

    const responseBonds = await this.tinkoffWebServices.getAllBonds(
      token,
      sandbox
    );
    milList.total += responseBonds.payload.total;
    milList.instruments.push(responseBonds.payload.instruments);
    return milList;
  }
  public async retrieveAllStocks(
    token: string,
    sandbox: boolean
  ): Promise<MarketInstrumentList> {
    let milList: MarketInstrumentList = { total: 0, instruments: [] };
    //read all bonds

    const responseStocks = await this.tinkoffWebServices.getAllStocks(
      token,
      sandbox
    );
    milList.total += responseStocks.payload.total;
    milList.instruments.push(responseStocks.payload.instruments);
    return milList;
  }
  public async retrieveAllEtfs(
    token: string,
    sandbox: boolean
  ): Promise<MarketInstrumentList> {
    let milList: MarketInstrumentList = { total: 0, instruments: [] };
    //read all bonds

    const responseEtfs = await this.tinkoffWebServices.getAllETFs(
      token,
      sandbox
    );
    milList.total += responseEtfs.payload.total;
    milList.instruments.push(responseEtfs.payload.instruments);
    return milList;
  }
  public async retrieveAllCurrencies(
    token: string,
    sandbox: boolean
  ): Promise<MarketInstrumentList> {
    let milList: MarketInstrumentList = { total: 0, instruments: [] };
    //read all bonds

    const responseCurrencies = await this.tinkoffWebServices.getAllCurrencies(
      token,
      sandbox
    );
    milList.total += responseCurrencies.payload.total;
    milList.instruments.push(responseCurrencies.payload.instruments);
    return milList;
  }


  public async retrieveAllMarketInstruments(
    token: string,
    sandbox: boolean
  ): Promise<MarketInstrumentList> {
    let milList: MarketInstrumentList = { total: 0, instruments: [] };
    //read all bonds

    const responseBonds = await this.tinkoffWebServices.getAllBonds(
      token,
      sandbox
    );
    milList.total += responseBonds.payload.total;
    milList.instruments.push(responseBonds.payload.instruments);

    // readall stocks
    const responseStocks = await this.tinkoffWebServices.getAllStocks(
      token,
      sandbox
    );
    milList.total += responseStocks.payload.total;
    milList.instruments.push(responseStocks.payload.instruments);
    // read all etfs
    const responseEtfs = await this.tinkoffWebServices.getAllETFs(
      token,
      sandbox
    );
    milList.total += responseEtfs.payload.total;
    milList.instruments.push(responseEtfs.payload.instruments);
    //read all currencies
    const responseCurrencies = await this.tinkoffWebServices.getAllCurrencies(
      token,
      sandbox
    );
    milList.total += responseCurrencies.payload.total;
    milList.instruments.push(responseCurrencies.payload.instruments);
    return milList;
  }

  public async getTinkoffActiveOrders(
    token: string,
    accountId: string,
    sandbox: boolean
  ) {
    return (
      await this.tinkoffWebServices.getActiveOrders(token, accountId, sandbox)
    ).payload;
  }

  public async getFigiByTicker(

    ticker: string,
    token: string,
    sandbox: boolean
  ) {
    return (
      await this.tinkoffWebServices.getFigiByTicker(ticker, token, sandbox)
    ).payload.instruments[0].figi;
  }

  public async tinkoffLimitOrder(
    tinkoffOperation: TinkoffOperation,
    account: TinkoffAccount,
    token: string,
    sandbox: boolean
  ) {
    return this.tinkoffWebServices.limitOrder(
      tinkoffOperation,
      account,
      token,
      sandbox
    );
  }
  public async getTickerByFigi(figi, token, sandbox): Promise<any> {
    return this.tinkoffWebServices.getTickerByFigi(figi, token, sandbox);
  }

  public async registerSandbox(token) {
    return this.tinkoffWebServices.registerSandbox(token);
  }
  public async removeSandboxAccount(brokerAccountId: string, token: string) {

    return this.tinkoffWebServices.removeSandboxAccount(brokerAccountId, token);

  }

  public async clearSandboxAccount(brockerAccountId: string, token: string) {

    return this.tinkoffWebServices.clearSandbox(brockerAccountId, token);



  }


  public async setSandboxBalance(brockerAccountId: string, currency: string, balance: number, token: string) {

    return this.tinkoffWebServices.setSandboxBalace(brockerAccountId, currency, balance, token);
  }


  public async getPortfolio(brockerAccountId: string, token: string, sandbox: boolean) {
    return this.tinkoffWebServices.getPortfolio(brockerAccountId, token, sandbox)
  }
  public async getCurrencies(brockerAccountId: string, token: string, sandbox: boolean):Promise<PortfolioCurrenciesResponse> {
    return this.tinkoffWebServices.getCurrencies(brockerAccountId, token, sandbox)
  }


  public async getCandles(figi:string, fromtimestamp:string, totimestamp:string, interval:string, token:string, sandbox: boolean): Promise<CandlesResponse>{
    return this.tinkoffWebServices.getCandles(figi,fromtimestamp,totimestamp,interval,token,sandbox) as Promise<CandlesResponse>;

  }
}
