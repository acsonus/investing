import OpenAPI from "@tinkoff/invest-openapi-js-sdk";
import {
    Candle,
    CurrencyPosition,
    MarketInstrument,
    Order,
    PortfolioPosition
} from "@tinkoff/invest-openapi-js-sdk/build/domain";
import {StockExchangeTime} from "../objects/businessobjects/common/StockExchangeTime";
import * as http from "http";
import {parse} from 'node-html-parser';
import {IncomingMessage} from "http";


const API_KEY = 'YOUR_API_KEY_HERE';
const API_SECRET = 'YOUR_API_SECRET_HERE';
const PAPER = true;

export class MeanReversion {
    //private alpaca:Alpaca;
    private runningAverage: number;
    private lastOrder;
    private openAPI: OpenAPI;
    private timeToClose: number;
    private ticker: string;
    private stock: string;
    private stockEXTime: StockExchangeTime;


    // private rusOpenTime: Date;
    // private rusCloseTime: Date;
    // private amOpenTime: Date = ;
    // private amCloseTime: Date;

    constructor(openAPI: OpenAPI, ticker: string) {

        this.runningAverage = 0;
        this.lastOrder = null;
        this.timeToClose = null;
        this.openAPI = openAPI;
        // Stock that the algo will trade.
        let isWinterTime = !this.isDST();
        // next time chage valid for usa
        //https://www.timeanddate.com/time/change/usa
        this.stockEXTime = new StockExchangeTime(isWinterTime);
        this.ticker = ticker;

    }


    private stdTimezoneOffset = () => {
        var jan = new Date(new Date().getFullYear(), 0, 1);
        var jul = new Date(new Date().getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    }

    private isDST() {
        return new Date().getTimezoneOffset() < this.stdTimezoneOffset();
    }


    async run() {
        //get figi of apple
        let mi: MarketInstrument = await this.openAPI.searchOne({ticker: this.ticker});

        this.stock = mi.figi;


        // First, cancel any existing orders so they don't impact our buying power.
        let orders: Order[] = await this.openAPI.orders();

        for (let order of orders) {
            await this.openAPI.cancelOrder({orderId: order.orderId}).catch((err) => {
                console.log(err.error);
            });
        }

        // Wait for market to open.
        console.log("Waiting for market to open...");
        var promMarket = this.awaitMarketOpen();
        await promMarket;
        console.log("Market opened.");

        let curCandles;
        // Get the running average of prices of the last 20 minutes, waiting until we have 20 bars from market open.
        let promBars = new Promise<void>((resolve, reject) => {
            let barChecker = setInterval(() => {
                    this.openAPI.candlesGet({
                        from: new Date(new Date().getTime()-21*60*1000).toISOString(),
                        to: new Date(new Date().getTime()).toISOString(), figi: this.stock, interval: "1min"
                    }).then(value => {
                        console.log(value.candles.length);
                        if (value.candles.length >= 20) {
                            curCandles = value.candles;
                            clearInterval(barChecker);
                            resolve();
                        }
                    }).catch((reason) => console.log((reason)));

                    // await this.alpaca.getCalendar(Date.now()).then(async (resp) => {
                    //     var marketOpen = resp[0].open;
                    //     await this.alpaca.getBars('minute', this.stock, {start: marketOpen}).then((resp) => {
                    //         var bars = resp[this.stock];
                    //         if (bars.length >= 20) {
                    //             clearInterval(barChecker);
                    //             resolve();
                    //         }
                    //     }).catch((err) => {
                    //         console.log(err.error);
                    //     });
                    // });
                },
                5000
                )
            ;
        });
        console.log("Waiting for 20 bars...");
        await promBars;
        console.log("We have 20 bars.");
        // Rebalance our portfolio every minute based off running average data.
        var spin = setInterval(async () => {
            // Clear the last order so that we only have 1 hanging order.
            if (this.lastOrder != null) await this.openAPI.cancelOrder(this.lastOrder.figi).catch((err) => {
                console.log(err.error);
            });
            // Figure out when the market will close so we can prepare to sell beforehand.
            var closingTime;
            var currTime;
            // await this.alpaca.getClock().then((resp) => {
            //     closingTime = new Date(resp.next_close.substring(0, resp.next_close.length - 6));
            //     currTime = new Date(resp.timestamp.substring(0, resp.timestamp.length - 6));
            // }).catch((err) => {
            //     console.log(err.error);
            // });


            this.timeToClose = this.stockEXTime.getSpbCloseTime().getTime() - new Date().getTime();
            console.log("time to close: "+this.millisToHoursMinutesAndSeconds(this.timeToClose));
            if (this.timeToClose < (60000 * 15)) {
                // Close all positions when 15 minutes til market close.
                console.log("Market closing soon.  Closing positions.");
                try {
                    this.openAPI.portfolio().then(portfolio => {

                        return portfolio.positions;
                    }).then(portfolioPositions => {
                        for (let pp of portfolioPositions) {
                            this.openAPI.marketOrder({figi: pp.figi, lots: pp.lots, operation: "Sell"})
                        }
                    })

                    // await this.alpaca.getPosition(this.stock).then(async (resp) => {
                    //
                    //
                    //     var positionQuantity = resp.qty;
                    //     var promOrder = this.submitMarketOrder(positionQuantity, this.stock, "sell");
                    //     await promOrder;
                    // }).catch((err) => {
                    //     console.log(err.error);
                    // });
                } catch (err) {/*console.log(err.error);*/
                }
                clearInterval(spin);
                console.log("Sleeping until market close (15 minutes).");
                setTimeout(() => {
                    // Run script again after market close for next trading day.
                    this.run();
                }, 60000 * 15);
            } else {
                // Rebalance the portfolio.
                await this.rebalance();
            }
        }, 60000);
    }

    // Spin until the market is open
    awaitMarketOpen() {
        let prom = new Promise<void>((resolve, reject) => {
            let isOpen = false;
            let marketChecker = setInterval(() => {
                let currTime = new Date().getTime();
                console.log("tick every minute: " + new Date(currTime).toTimeString())
                if (currTime < this.stockEXTime.getSpbOpenTime().getTime()) {
                    const timeToOpen = this.stockEXTime.getSpbOpenTime().getTime() - currTime;
                    console.log("Time to open: " + this.millisToHoursMinutesAndSeconds(timeToOpen));
                } else {
                    clearInterval(marketChecker);
                    resolve();
                }

            }, 5000);
        });
        return prom;
    }


    private millisToHoursMinutesAndSeconds(millis): string {

        var minutes = Math.floor(millis / 60000);
        var hours = Math.floor(minutes / 60);
        minutes = minutes - hours * 60;
        var seconds = ((millis % 60000) / 1000).toFixed(0);


        return hours + ":" + minutes + ":" + seconds;
    }


    // Rebalance our position after an update.
    async rebalance() {
        var positionQuantity = 0;
        var positionValue = 0;

        // Get our position, if any.
        try {


            this.openAPI.portfolio().then(portfolio => {
                return portfolio.positions;
            }).then(portfolioPositions => {
                    return portfolioPositions.filter(value => {
                        return (value.figi == this.stock)
                    })[0];
                }
            ).then(value => {
                positionQuantity = value.lots;
                positionValue = value.balance;//?
            });
            // await this.alpaca.getPosition(this.stock).then((resp) => {
            //     positionQuantity = resp.qty;
            //     positionValue = resp.market_value;
            // });
        } catch (err) {/*console.log(err.error);*/
        }

        // Get the new updated price and running average.
        var bars: Candle[];

        this.openAPI.candlesGet({
            from: new Date(new Date().getTime() - 20 * 60 * 1000).toISOString(),
            to: new Date().toISOString(),
            figi: this.stock,
            interval: "1min"
        }).then(candles => {
            bars = candles.candles;
        });
        // await this.alpaca.getBars('minute', this.stock, {limit: 20}).then((resp) => {
        //     bars = resp[this.stock];
        // }).catch((err) => {
        //     console.log(err.error);
        // });
        var currPrice = bars[bars.length - 1].c;
        this.runningAverage = 0;
        bars.forEach((bar) => {
            this.runningAverage += bar.c;
            //this.runningAverage += bar.closePrice;
        })
        this.runningAverage /= 20;

        if (currPrice > this.runningAverage) {
            // Sell our position if the price is above the running average, if any.
            if (positionQuantity > 0) {
                console.log("Setting position to zero.");


                await this.submitLimitOrder(positionQuantity, this.stock, currPrice, 'sell');
            } else console.log("No position in the stock.  No action required.");
        } else if (currPrice < this.runningAverage) {
            // Determine optimal amount of shares based on portfolio and market data.
            var portfolioValue;
            var buyingPower;
            //stopped here
            // Buying power equals the total cash held in the brokerage account plus all available margin.
            await this.openAPI.portfolioCurrencies().then(portfolioCurrencies => {
                let currencyPositions: CurrencyPosition[] = portfolioCurrencies.currencies;
                buyingPower = currencyPositions.filter(value => {
                    return (value.currency == "USD");
                })[0].balance;
            });

            // await this.alpaca.getAccount().then((resp) => {
            //     portfolioValue = resp.portfolio_value;
            //     buyingPower = resp.buying_power;
            // }).catch((err) => {
            //     console.log(err.error);
            // });
            var portfolioShare = (this.runningAverage - currPrice) / currPrice * 200;
            var targetPositionValue = portfolioValue * portfolioShare;
            var amountToAdd = targetPositionValue - positionValue;

            // Add to our position, constrained by our buying power; or, sell down to optimal amount of shares.
            if (amountToAdd > 0) {
                if (amountToAdd > buyingPower) amountToAdd = buyingPower;
                var qtyToBuy = Math.floor(amountToAdd / currPrice);
                await this.submitLimitOrder(qtyToBuy, this.stock, currPrice, 'buy');
            } else {
                amountToAdd *= -1;
                var qtyToSell = Math.floor(amountToAdd / currPrice);
                if (qtyToSell > positionQuantity) qtyToSell = positionQuantity;
                await this.submitLimitOrder(qtyToSell, this.stock, currPrice, 'sell');
            }
        }
    }

    // Submit a limit order if quantity is above 0.

    async submitLimitOrder(quantity, stock, price, side) {
        if (quantity > 0) {
            await this.openAPI.limitOrder({
                figi: stock,
                operation: side,
                lots: quantity,
                price: price
            }).then((placeLimitOrder) => {
                    console.log("TinkoffOrder for " + stock + " for " + quantity + " lots is in status " + placeLimitOrder.status)
                }
            ).catch((err) => {
                console.log("Scheisse passiert ")
            });
            // await this.alpaca.createOrder({
            //     symbol: stock,
            //     qty: quantity,
            //     side: side,
            //     type: 'limit',
            //     time_in_force: 'day',
            //     limit_price: price
            // }).then((resp) => {
            //     this.lastOrder = resp;
            //     console.log("Limit order of |" + quantity + " " + stock + " " + side + "| sent.");
            // }).catch((err) => {
            //     console.log("TinkoffOrder of |" + quantity + " " + stock + " " + side + "| did not go through.");
            // });
        } else {
            console.log("Quantity is <=0, order of |" + quantity + " " + stock + " " + side + "| not sent.");
        }
    }

    // Submit a market order if quantity is above 0.

    async submitMarketOrder(quantity, stock, side) {


        if (quantity > 0) {

            // mean startegy

            await this.openAPI.marketOrder({figi: stock, operation: side, lots: quantity}).then((placedMarketOrder) => {
                    console.log("Market order of |" + quantity + " " + stock + " " + side + "| completed with status " + placedMarketOrder.status);
                }
            ).catch((err) => {
                console.log("TinkoffOrder of |" + quantity + " " + stock + " " + side + "| did not go through.");
            })


            // await this.alpaca.createOrder({
            //     symbol: stock,
            //     qty: quantity,
            //     side: side,
            //     type: 'market',
            //     time_in_force: 'day'
            // }).then((resp) => {
            //     this.lastOrder = resp;
            //     console.log("Market order of |" + quantity + " " + stock + " " + side + "| completed.");
            // }).catch((err) => {
            //
            // });
        } else {
            console.log("Quantity is <=0, order of |" + quantity + " " + stock + " " + side + "| not sent.");
        }
    }
}
