import fs from 'fs';
import path from "path";

export class Configutils {


    constructor(private file: File) {
    }

    static getProdConfig(): string {
        return fs.readFileSync(path.resolve(__dirname, '../credentials/prod.json'), 'utf-8');
    }

    static getSandboxConfig(): string {
        return fs.readFileSync(path.resolve(__dirname, '../credentials/sandbox.json'), 'utf-8');
    }

}
