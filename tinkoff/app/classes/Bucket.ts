import {Order} from "@tinkoff/invest-openapi-js-sdk/build/domain";

export class Bucket {
    executed: Order[];
    incomplete: Order[];
    constructor(executed:Order[], incomplete:Order[]) {
        this.executed=executed;
        this.incomplete= incomplete;
    }

}