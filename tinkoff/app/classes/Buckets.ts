import {Bucket} from "./Bucket";

export class Buckets {
    longOrders: Bucket;
    shortOrders: Bucket;

    constructor(longOrders: Bucket, shortOrders: Bucket) {


        this.longOrders = longOrders;
        this.shortOrders = shortOrders;
    }
}