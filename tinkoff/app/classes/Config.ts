export class Config {
    apiURL: string;
    socketURL: string;
    token: string;
}