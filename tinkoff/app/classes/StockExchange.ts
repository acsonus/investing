export class StockExchangeTime {
    private summertime: boolean;
    private timeCorrection;

    /*
    if time is winter time eastern time is shifted by time of time Corretion variable
     */
    constructor(winterTime: boolean) {
        this.timeCorrection = (winterTime) ? 1 : 0;


    }

    public getUSPreMarketOpenTime() {
        let d = new Date;
        d.setHours(14 + this.timeCorrection, 30, 0, 0);
        return d;
    }

    //winter time winter if true it is winter time
    public getUSMarketOpenTime(winter: boolean) {
        let d = new Date;

        d.setHours(16 + this.timeCorrection, 30, 0, 0);
        return d;
    }

    public getUSCloseTime() {
        let d = new Date();
        d.setHours(23 + this.timeCorrection, 0, 0, 0);
        return d;
    }

    public getUSPostMarketCloseTime() {
        let d = new Date();
        d.setHours(1, 40, 0, 0);
        return d;
    }


    public getMoexOpenTime() {
        let d = new Date();
        d.setHours(10, 0, 0, 0);
        return d;
    }

    public getMoexCloseTim() {
        let d = new Date();
        d.setHours(19, 30, 0, 0);
        return d;
    }


    public getSpbOpenTime() {
        let d = new Date();
        d.setHours(10,0,0,0);
        return d;
    }

    public getSpbCloseTime() {
        let d = new Date();
        d.setDate(d.getDate()+1);
        d.setHours(1, 40, 0, 0);
        return d;
    }


}
