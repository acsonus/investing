import express from "express";
import bodyParser from 'body-parser' ;
import {Financialwebservices} from "./webservices/financialwebservices";


const app = express();
app.use(bodyParser.json()); // to support JSON-encoded bodies
// start body-parser configuration
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

app.use( (req, res, next) =>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const finserv:Financialwebservices = new Financialwebservices();
// finserv.getAppleRates(app);
// finserv.runLongShortTS(app);
// finserv.runMeanTS(app);

const port = 3000;
const server = app.listen(port, () => {
    console.log("Server app listening at localhost", port)
});
