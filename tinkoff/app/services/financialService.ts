import OpenAPI from '@tinkoff/invest-openapi-js-sdk';

import {Candle, Candles, MarketInstrument} from "@tinkoff/invest-openapi-js-sdk/build/domain";
import {Configutils} from "../utils/configutils";
import {Config} from "../classes/Config";
import {LongShort} from "../strategies/LongShortStrategy";
import {MeanReversion} from "../strategies/MeanRevisionStrategy";

/**
 *
 * o цена открытия
 * c цена закрытия
 * h наибольшая цена
 * l наименьшая цена
 * v объем торгов
 */

export class FinancialServices {

    static async getConnection(production: boolean): Promise<OpenAPI> {
        const prodCreds: string = Configutils.getProdConfig();
        const sbCreds: string = Configutils.getSandboxConfig();

        const config: Config = JSON.parse((production) ? prodCreds : sbCreds);
        console.log(config);
        return new OpenAPI({apiURL: config.apiURL, secretToken: config.token, socketURL: config.socketURL});
    }

    static async getAppleRates(req: any, res: any) {
        const body = req.body;
        const environment = body.environment;
        const api = await this.getConnection(environment);
        const figi: MarketInstrument | null = await api.searchOne({ticker: body.ticker});

        const candlesObj: Candles = await api.candlesGet({
            from: '2020-08-19T18:38:33.131642+03:00',
            to: '2020-08-19T18:48:33.131642+03:00',
            figi: figi.figi,
            interval: '5min',
        }) // Получаем свечи за конкретный промежуток времени.

        const result = new Array<Candle>()
        for (const candle of candlesObj.candles) {
            console.log("Currently displaying " + candle.figi);
            console.log("Candle high: " + candle.h)
            result.push(candle);
        }


    }

    static async runBasicOperations(req: any, res: any) {
        const body = req.body;
        const environment = body.environment;
        let production: boolean = (environment == "production") ? true : false;

        const api: OpenAPI = await this.getConnection(production);
        const portfolio = await api.portfolio();
        console.log(portfolio);


    }

    //Run strategy long short
    static async runStratagyLongShort(req: any, res: any) {
        const body = req.body;
        const environment = body.environment;
        let production: boolean = (environment == "production") ? true : false;
        const api: OpenAPI = await this.getConnection(production);
        let ls: LongShort = new LongShort(api);
        await ls.run()

    }

    //Run strategy mean revision
    static async runStratagyMeanrevision(req: any, res: any) {
        const body = req.body;
        const ticker = body.ticker;
        const environment = body.environment;
        const api: OpenAPI = (environment == "production") ? await this.getConnection(true) : await this.getConnection(false);
        let mr: MeanReversion = new MeanReversion(api, ticker);
        res.send("{\"message\":\"wrong environmet variable\"}");
        await mr.run()
    }


}
