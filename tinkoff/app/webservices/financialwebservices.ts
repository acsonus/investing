import {FinancialServices} from "../services/financialService";
import {Express} from "express";


export class Financialwebservices {
    async getAppleRates(app: Express) {
        return app.post('/api/financialwebservices/getCandles', (req, res) => {
            console.log("getAppleRates method is called")
            FinancialServices.getAppleRates(req, res);
        });
    }

    async runLongShortTS(app: Express) {
        return app.post('/api/financialwebservices/runLS', (req, res) => {
            console.log("runStratagyLongShort method is called")
            FinancialServices.runStratagyLongShort(req, res);
            res.send({"status":"sucess"});
        });
    }

    async runMeanTS(app: Express) {
        return app.post('/api/financialwebservices/runMR', async (req, res) => {
            console.log("runStratagyMeanrevision method is called")
            await FinancialServices.runStratagyMeanrevision(req, res);
            res.send({"status":"sucess"});
        });
    }
    async runTest(app: Express) {
        return app.post('/api/financialwebservices/runBasicOperations', async (req, res) => {
            console.log("runBasicOperations method is called")
            await FinancialServices.runBasicOperations(req, res);
            res.send({"status":"sucess"});
        });
    }



}
