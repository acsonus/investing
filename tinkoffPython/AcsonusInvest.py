from datetime import datetime
import matplotlib.pyplot as plt
import tinvest
from tinvest import MarketInstrument, Currency, SyncClient, MarketInstrumentListResponse, CandleResolution

import TradingBot;

TOKEN = "t.IkWaJlE574n-pPtRFwrY02JMx1ZF5JrXvAgKoLzzxb5DP9bu7CjQu-DoLgzpovc7ITacSbkJV7T8_9n-l5rPhw"
allUSSocks = [];
allRuStocks = [];
allEurStocks = [];
tinkoffSyncClient: SyncClient = None


def printPortfolio():
    tinkoffClient = tinvest.SyncClient(TOKEN)
    response = tinkoffClient.get_portfolio()  # tinvest.PortfolioResponse
    print(response.payload)


def getCandlesToStock(ticker, fromTime, toTime):
    tinkoffSyncClient = tinvest.SyncClient(TOKEN)
    instrumentListResponse: MarketInstrumentListResponse = tinkoffSyncClient.get_market_search_by_ticker(ticker);
    if instrumentListResponse.payload.instruments and len(instrumentListResponse.payload.instruments) == 1:
        mi: MarketInstrument = instrumentListResponse.payload.instruments[0]
        response = tinkoffSyncClient.get_market_candles(mi.figi, fromTime, toTime, CandleResolution("day"))
        return response;


def loadAllStocks():
    tinkoffSyncClient = tinvest.SyncClient(TOKEN)
    marketInstruments = tinkoffSyncClient.get_market_stocks().payload.instruments
    # print(marketInstruments)
    for mi in marketInstruments:
        # print("type of mi is ", type(mi));
        if (mi.currency == Currency.usd):
            allUSSocks.append(mi);
        if (mi.currency == Currency.rub):
            allEurStocks.append(mi);
        if (mi.currency == Currency.eur):
            allRuStocks.append(mi);


def listAllStocks(stocktype):
    if (stocktype == Currency.usd):
        printAllStocks(allUSSocks)
    if (stocktype == Currency.rub):
        printAllStocks(allRuStocks)
    if (stocktype == Currency.eur):
        printAllStocks(allEurStocks)


def printAllStocks(stocksarray):
    for mi in stocksarray:
        print(mi)


def revealStocksByCurrency(currency):
    if (currency == Currency.usd):
        return allUSSocks
    if (currency == Currency.rub):
        return allRuStocks
    if (currency == Currency.eur):
        return allEurStocks


def trade():
    algorithm = "MeanAverage"
    global tinkoffSyncClient
    tb = TradingBot(TOKEN, tinkoffSyncClient, algorithm);


if __name__ == '__main__':
    # printPortfolio()
    # loadAllStocks()
    # listAllStocks(Currency.usd)
    # currentAsset = revealStocksByCurrency(Currency.usd)

    fromTime = datetime(2021, 1, 1, 12, 00, 00)
    toTime = datetime(2021, 4, 15, 12, 00, 00)
    response = getCandlesToStock('BA', fromTime, toTime)
    print("Всего свечей :" + str(len(response.payload.candles)))
    # o - open price c - close price h - highest price l lovest price v - trade volumes
    xArray = [];
    yArray = [];
    counter: int = 0
    for candle in response.payload.candles:
        counter += 1
        xArray.append(counter)
        yArray.append((candle.h + candle.l) / 2)

    for candle in response.payload.candles:
        print(candle)
    plt.plot(xArray, yArray)
    plt.axis([0, 75, 0, 300])
    plt.show()
