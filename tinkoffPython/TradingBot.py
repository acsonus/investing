from edu.alexander.tradingStrategies.MeanRevisionStrategy import MeanRevisionStrategy


class TradingBot:
    currentStrategyName = ""
    currentStrategy = None
    tinkoffClient = None
    def __init__(self, client, currentStrategyName):
        if client != None:
            print("everything is ok let us start")
            self.tinkoffClient = client
            self.currentStrategyName = currentStrategyName
    def startrading(self):
        self.chooseStrategy("MeanRevision")

    def choosestrategy(self,strategy):
        print("Strategy chosen:"+strategy)
        if strategy=="MeanRevision":
            currentStrategy = MeanRevisionStrategy(self.tinkoffClient)

